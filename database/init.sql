-- drop database if exists and (re)create it
DROP DATABASE IF EXISTS projet_fabi_barrault;
CREATE DATABASE projet_fabi_barrault;
USE projet_fabi_barrault;

-- (re)create user
DROP USER IF EXISTS 'user_fabi_barrault'@'localhost';
CREATE USER 'user_fabi_barrault'@'localhost' IDENTIFIED BY 'fabi_barrault';
GRANT ALL PRIVILEGES ON projet_fabi_barrault . * TO 'user_fabi_barrault'@'localhost';
FLUSH PRIVILEGES;

-- create tables
CREATE TABLE users (
	uid INTEGER AUTO_INCREMENT PRIMARY KEY,
	lastname text not null,
	firstname text not null,
	password text not null,
	email varchar(255) not null UNIQUE
);

CREATE TABLE ingredients (
	iid INTEGER AUTO_INCREMENT PRIMARY KEY,
	name TEXT NOT NULL,
  add_date DATE,
  uid INTEGER,
  FOREIGN KEY (uid)
        REFERENCES users(uid)
        ON DELETE SET NULL
);

CREATE TABLE user_ingredients (
  uid INTEGER,
  iid INTEGER,
  FOREIGN KEY (uid)
        REFERENCES users(uid)
        ON DELETE CASCADE,
  FOREIGN KEY (iid)
        REFERENCES ingredients(iid)
        ON DELETE CASCADE,
  ingr_number INTEGER,
  PRIMARY KEY(uid, iid)
);

CREATE TABLE recipe (
	rid INTEGER AUTO_INCREMENT PRIMARY KEY,
	name TEXT NOT NULL,
  add_date DATE,
  uid INTEGER,
  FOREIGN KEY (uid)
        REFERENCES users(uid)
        ON DELETE SET NULL
);

CREATE TABLE recipe_ingredients (
  rid INTEGER,
  iid INTEGER,
  FOREIGN KEY (rid)
        REFERENCES recipe(rid)
        ON DELETE CASCADE,
  FOREIGN KEY (iid)
        REFERENCES ingredients(iid)
        ON DELETE CASCADE,
  ingr_number INTEGER,
  PRIMARY KEY(rid, iid)
);


-- Populate database

INSERT INTO users (
  lastname, firstname, password, email
) VALUES (
  "Potter", "Harry", "nimbus2000", "harrypotter@gryffindor.hp"
);

INSERT INTO users (
  lastname, firstname, password, email
) VALUES (
  "Granger", "Hermione", "viveleselfs", "hermionegranger@gryffindor.hp"
);

INSERT INTO ingredients (
  name, add_date, uid
) VALUES (
  "Tomate", NOW(), 1
);

INSERT INTO ingredients (
  name, add_date, uid
) VALUES (
  "Steack", NOW(), 1
);

INSERT INTO ingredients (
  name, add_date, uid
) VALUES (
  "Pates", NOW(), 1
);

INSERT INTO user_ingredients (
	uid, iid, ingr_number
) VALUES (
	1, 1, 1
);

INSERT INTO user_ingredients (
	uid, iid, ingr_number
) VALUES (
	1, 2, 1
);

INSERT INTO user_ingredients (
	uid, iid, ingr_number
) VALUES (
	1, 3, 1
);

INSERT INTO recipe (
	rid, name, add_date, uid
) VALUES (
	1, "Pates bolognèses", NOW(), 1
);

INSERT INTO recipe_ingredients (
	rid, iid, ingr_number
) VALUES (
	1, 1, 1
);

INSERT INTO recipe_ingredients (
	rid, iid, ingr_number
) VALUES (
	1, 2, 1
);

INSERT INTO recipe_ingredients (
	rid, iid, ingr_number
) VALUES (
	1, 3, 1
);
