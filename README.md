# projet-pd1

Projet de Programmation Distribuée !

Il y a deux utilisateurs sur le site :   
Harry Potter et Hermione Granger.  
Le frido d'Hermione est vide, et le frigo d'Harry contient les ingredients nécéssaires pour faire des pates bolognèses (Pates, tomates, steack). Ces trois ingredients sont présents dans la base de donnée du site.  
Supprimer un ingredients du frigo d'Harry permet de voir que le site ne lui propose plus la recette.  
Les urls du front sont les suivantes :  
/ -> La page de base, donne la recette possible et la liste des ingredients du frigo    
/account -> affiche la liste des utilisateurs présents sur le site   
/ingredients -> Affiche la liste des ingredients disponibles sur le site   
/recipe -> Affiche la liste des recettes disponibles sur le site   
