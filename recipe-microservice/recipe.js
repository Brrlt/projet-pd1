/**
setting up the server
**/
const bodyParser = require("body-parser"); //module sert à décomposer les requêtes HTTP pour extraire les infos
const express = require('express'); //importe le module express (framework)
const server = express(); //crée une application express
const cors = require('cors');
const mysql = require("mysql2");

server.use(cors());
server.use(bodyParser.urlencoded({ extended: false }));
server.use(bodyParser.json());

/**
connection to database
**/
//importe le module mysql et crée une  connexion à la base de données
const connection = mysql.createConnection({
	host : 'db',
	user : 'user_fabi_barrault',
	password : 'fabi_barrault',
	database : 'projet_fabi_barrault'
});
connection.connect();


/**
Gestion générale des recettes
**/
server.get("/recette", (req, res) => {
  connection.query("SELECT name FROM recipe;", (err, rows, col) => {
    if (err) {
      res.send({"Error" : true});
    } else if (rows.length <= 0) {
      res.send({"Error : No rows" : true});
    } else {
      res.json(rows);
    }
  });
});

server.get("/recette/:name", (req, res) => {
  connection.query("SELECT name FROM recipe where name= ?;", [req.params.name], (err, rows, col) => {
    if (err) {
      res.send({"Error" : true});
    } else if (rows.length <= 0) {
      res.send({"Error : No rows" : true});
    } else {
      res.json(rows);
    }
  });

});

server.delete("/recette/:name", (req, res) => {
  connection.query("DELETE FROM recipe WHERE name= ?;", [req.params.name], (err, result) => {
    if (err) {
      res.send({"Error" : true});
    }
  });
});

server.post("/recette", (req, res) => {
  console.log("hey")
  connection.query("SELECT * FROM recipe WHERE name=?;", [req.body.name], (err, rows, col) => {
    if (err) {
      res.send({"Error" : true});
    } else if (rows.length <= 0) {
      connection.query("INSERT INTO recipe (name, add_date, uid) VALUES (?, ?, ?);", [req.body.name, "2021-12-20", 1], (err, result) => {
        if (err) {
          res.send({"Error" : true});
        }
      });
    } else {
      res.send({"A recipe with the same name already exists" : true});
    }
  });
});

/**
Calculateur de recettes
**/
 server.get("/user/:name", (req, res) => {
   var query1 = "SELECT i.name FROM ingredients i JOIN user_ingredients ui ON i.iid = ui.iid JOIN users u ON ui.uid = u.uid WHERE u.firstname = ?;";

   connection.query(query1, [req.params.name], (err, rows, col) => {
     if (err) {
       res.send({"Error" : true});
     } else if (rows.length <= 0) {
       res.send({"Vous n'avez pas d'ingrédients." : true});
     } else {
			 var ingredients_list1 = [];
			 rows.forEach(item => ingredients_list1.push(item["i.name"]));
       var nrows = 1;
       connection.query("SELECT name, rid FROM recipe;", (err, rows, col) => {
         if (err) {
           res.send({"Error" : true});
         } else if (rows.length =! 0) {
					 var recipe_list = [];
					 rows.forEach(item => recipe_list.push([item["name"], item["rid"]]));

					 // pour chacune des recettes on regarde si sa liste d'ingrédients est inclue dans celle de celui de l'utilisateur
					 var r_list = [];
				   calculrecette(res, recipe_list, ingredients_list1, r_list);

         }
       });
     }
   });
 });

 server.listen(8080, () =>{
	 console.log(`Server listening on port 8080`);
 });

 function calculrecette(res, recipe_list, ingredients_list1, r_list){
		 for (const array of recipe_list) {
			 var query2 = "SELECT i.name FROM ingredients i JOIN recipe_ingredients ri ON i.iid = ri.iid WHERE ri.rid = ?;";
			 var flag = false;
			 connection.query(query2, array[1], (err, rows, col) => {
					if (err || rows.length <=0) {
						flag = true;
					} else {
						var ingredients_list2 = [];
						rows.forEach(item => ingredients_list2.push(item["i.name"]));
						var bool = [];
						for (const name of ingredients_list1) {
							for(const name2 of ingredients_list2) {
								if (name === name2){
									bool.push(1);
									break;
								}
							}
							if(bool.length == ingredients_list2.length){
								r_list.push(array[0]);
								res.send(JSON.stringify(r_list));
								break;
							}
						}
					}
			 });
		 }
 }
