const express = require("express")
var server = express()
var cors = require('cors')
const bodyParser = require("body-parser");
const mysql = require("mysql2");
const connection = mysql.createConnection({
	host : 'db',
	user : 'user_fabi_barrault',
	password : 'fabi_barrault',
	database : 'projet_fabi_barrault'
});
connection.connect();

server.use(cors())
server.use(bodyParser.urlencoded({ extended: false }));
server.use(bodyParser.json());

/**
* Gestion générale des ingredients
*/
server.get("/", function(req, res) {
	connection.query("SELECT * FROM ingredients;", function(err, rows, fields) {
			if (err || !rows || rows.length <= 0) {
				if (err) console.log(err);
				res.send({"error": true});
			}
			else {
				res.json(rows);
			}
		});
});

/**
* Gestion de la liste des ingredients
*/
server.get("/:name", function(req, res) {
	connection.query("SELECT * FROM ingredients WHERE name= ?;", [req.params.name], function(err, rows, fields) {
			if (err || !rows || rows.length <= 0) {
				if (err) console.log(err);
			  res.send({"error": true});
			}
			else {
				res.json(rows);
			}
		});
});

server.get("/:id", function(req, res) {
	connection.query("SELECT * FROM ingredients WHERE id = ?;", [req.params.id], function(err, rows, fields) {
			if (err || !rows || rows.length <= 0) {
				if (err) console.log(err);
			  res.send({"error": true});
			}
			else {
				res.json(rows);
			}
		});
});

server.post("/", function(req, res) {
	connection.query("SELECT * FROM ingredients WHERE name = ?;",[req.body.name], function(err, rows, fields) {
			if (err || !rows ) {
				if (err) console.log(err);
				res.send({"error": true});
			}
			else if (rows.length == 0){
				connection.query("INSERT INTO ingredients (name, add_date, uid) VALUES (?, ?, ?)",
				[req.body.name, "2021-12-20", 1], function(err, rows, fields){
					if (err) {
						console.log(err);
						res.send({"error": true});
					}
					else{
						res.send({"error": false});
					}

				});
			}
			else {
				res.send({"error": true});
			}
		});
});

server.delete("/:name", function(req, res) {
	connection.query("DELETE FROM ingredients WHERE name = ?;",[req.params.name], function(err, rows, fields) {
			if (err || !rows ) {
				if (err) console.log(err);
				res.send({"error": true});
			}
			else {
				res.send({"error": false});
			}
	});
});

server.delete("/:id", function(req, res) {
	connection.query("DELETE FROM ingredients WHERE id = ?;",[req.params.id], function(err, rows, fields) {
			if (err || !rows ) {
				if (err) console.log(err);
				res.send({"error": true});
			}
			else {
				res.send({"error": false});
			}
	});
});

server.get("/user/:name", function(req, res) {
	connection.query("SELECT i.name FROM ingredients i JOIN user_ingredients ui ON i.iid = ui.iid JOIN users u ON ui.uid = u.uid WHERE u.firstname = ?;", [req.params.name], function(err, rows, fields) {
			if (err || !rows || rows.length <= 0) {
				if (err) console.log(err);
			  res.send("Error");
			}
			else {
				res.json(rows);
			}
		});
});

server.post("/user/:name", function(req, res) {
	connection.query("SELECT * FROM ingredients WHERE name = ?", [req.body.name], function(err, rows, fields) {
			if (err || !rows || rows.length != 1) {
				res.send({"error": true});
			}
			else if(rows.length >= 1) {
				connection.query("SELECT * FROM users WHERE firstname = ?", [req.params.name],function(err, rows_mail, fields) {
					if (err || !rows || rows.length != 1) {
						res.send({"error": true});
					}else{
						connection.query("SELECT * FROM ingredients WHERE name = ?", [req.body.name],function(err, rows_ingr, fields) {
							if (err || !rows || rows.length != 1) {
								res.send({"error": true});
							}else{
								uid = rows_mail[0]["uid"];
								iid = rows_ingr[0]["iid"];
								connection.query("INSERT INTO user_ingredients (uid, iid, ingr_number) VALUES (?, ?, ?) ;", [uid,iid,req.body.number], function(err, rows, fields) {
									if (err || !rows || rows.length <= 0) {
										if (err) console.log(err);
											res.send({"error": true});
										}
										else {
											res.json(rows);
										}
									});
							}
						});
					}
				});
			}
		});
});

server.delete("/user/:name/:ingr", function(req, res) {
	connection.query("SELECT * FROM ingredients WHERE name = ?", [req.params.ingr], function(err, rows, fields) {
			if (err || !rows || rows.length != 1) {
				res.send({"error": true});
			}
			else if(rows.length >= 1) {
				connection.query("SELECT * FROM users WHERE firstname = ?", [req.params.name],function(err, rows_mail, fields) {
					if (err || !rows || rows.length != 1) {
						res.send({"error": true});
					}else{
						connection.query("SELECT * FROM ingredients WHERE name = ?", [req.params.ingr],function(err, rows_ingr, fields) {
							if (err || !rows || rows.length != 1) {
								res.send({"error": true});
							}else{
								uid = rows_mail[0]["uid"];
								iid = rows_ingr[0]["iid"];
								connection.query("DELETE FROM user_ingredients WHERE uid = ? AND iid = ?;", [uid,iid], function(err, rows, fields) {
									if (err || !rows || rows.length <= 0) {
										if (err) console.log(err);
											res.send({"error": true});
										}
										else {
											res.json(rows);
										}
									});
							}
						});
					}
				});
			}
		});
});


server.listen(8081, function(){
	console.log("server started on port 8081")
});
