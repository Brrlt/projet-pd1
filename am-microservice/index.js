const express = require("express")
var server = express()
var cors = require('cors')
const bodyParser = require("body-parser");
const mysql = require("mysql2");
const connection = mysql.createConnection({
	host : 'db',
	user : 'user_fabi_barrault',
	password : 'fabi_barrault',
	database : 'projet_fabi_barrault'
});
connection.connect();
server.use(cors())
server.use(bodyParser.urlencoded({ extended: false }));
server.use(bodyParser.json());

server.get("/", function(req, res) {
	connection.query("SELECT * FROM users;", function(err, rows, fields) {
			if (err || !rows || rows.length <= 0) {
				if (err) console.log(err);
				res.send({"error": true});
			}
			else {
				res.json(rows);
			}
		});
});

server.get("/:email", function(req, res) {
	connection.query("SELECT * FROM users WHERE email= ?;", [req.params.email], function(err, rows, fields) {
			if (err || !rows || rows.length <= 0) {
				if (err) console.log(err);
				res.send({"error": true});
			}
			else {
				res.json(rows);
			}
		});
});

server.post("/", function(req, res) {
	connection.query("SELECT * FROM users WHERE email= ?;",[req.body.email], function(err, rows, fields) {
			if (err || !rows ) {
				if (err) console.log(err);
				res.send({"error": true});
			}
			else if (rows.length == 0){
				connection.query("INSERT INTO users (lastname, firstname, password, email) VALUES (?, ?, ?, ?)",
				[req.body.lastname, req.body.firstname, req.body.password, req.body.email], function(err, rows, fields){
					res.send({"error": false});
				});
			}
			else {
				res.send({"error": true});
			}
		});
});

/**
* WIP
*/
server.delete("/", function(req, res) {
	connection.query("SELECT * FROM users;", function(err, rows, fields) {
			if (err || !rows || rows.length <= 0) {
				if (err) console.log(err);
				res.send({"error": true});
			}
			else {
				res.json(rows);
			}
		});
});

server.patch("/", function(req, res) {
	connection.query("SELECT * FROM users;", function(err, rows, fields) {
			if (err || !rows || rows.length <= 0) {
				if (err) console.log(err);
				res.send({"error": true});
			}
			else {
				res.json(rows);
			}
		});
});


server.listen(8082, function(){
	console.log("server started")
});
