import { Component } from '@angular/core';
import { FormControl } from '@angular/forms';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-account-list',
  templateUrl: './account-list.component.html',
  styleUrls: ['./account-list.component.css']
})
export class AccountListComponent {

  constructor(private http: HttpClient) { }

users = null;
ingredients = null;
show_user_name = new FormControl('');
add_user_name = new FormControl('');
add_user_email = new FormControl('');
add_user_pass = new FormControl('');
add_user_firstname = new FormControl('');

 ngOnInit() {
     this.http.get<any>('http://172.23.1.4:8082').subscribe(data => {
         this.users = data;
     })
 }
 show_user(){
	 var url = 'http://172.23.1.2:8081/user/' + this.show_user_name.value
	 this.http.get<any>(url).subscribe(data => {
		 console.log(data)
		 this.ingredients = data;
	 })
 }

 add_user(){
	 console.log(this.add_user_name)
	 this.http.post<any>('http://172.23.1.4:8082', {"lastname": this.add_user_name.value, "firstname": this.add_user_firstname.value, "password": this.add_user_pass.value, "email": this.add_user_email.value   }).subscribe(data => {
	   this.http.get<any>('http://172.23.1.4:8082').subscribe(data => {
		   this.users = data;
	   })
	 })

   }
}
