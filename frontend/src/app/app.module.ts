import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouterModule } from '@angular/router';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

import { AppComponent } from './app.component';
import { TopBarComponent } from './top-bar/top-bar.component';
import { IngredientsListComponent } from './ingredients-list/ingredients-list.component';
import { RecipeListComponent } from './recipe-list/recipe-list.component';
import { AccountListComponent } from './account-list/account-list.component';
import { HomeComponent } from './home/home.component';
import { AppRoutingModule } from './app-routing.module';

@NgModule({
  imports: [
    BrowserModule,
    ReactiveFormsModule,
    HttpClientModule,
    AppRoutingModule
  ],
  declarations: [
    AppComponent,
    TopBarComponent,
    IngredientsListComponent,
    RecipeListComponent,
	HomeComponent,
    AccountListComponent
  ],
  bootstrap: [
    AppComponent
  ]
})
export class AppModule { }
