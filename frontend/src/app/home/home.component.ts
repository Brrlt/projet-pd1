import { Component } from '@angular/core';
import { FormControl } from '@angular/forms';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent {

  constructor(private http: HttpClient) {

   }

recipe = null;
ingredients = null;
new_ingredient = new FormControl('');
user = new FormControl('');

 show_recipe(){
	 this.recipe = null;
	 this.ingredients = null;
	 var url = 'http://172.23.1.3:8080/user/' + this.user.value
	 //var url = 'http://localhost:8080/user/' + this.user.value
	 this.http.get<any>(url).subscribe(data => {
		 this.recipe = data;
	 })
	 var url = 'http://172.23.1.2:8081/user/' + this.user.value
	 //var url = 'http://localhost:8081/user/' + this.user.value
	 this.http.get<any>(url).subscribe(data => {
		 this.ingredients = data;
	 })
 }

 add_ingr(){
	var url = 'http://172.23.1.2:8081/user/' + this.user.value
	//var url = 'http://localhost:8081/user/' + this.user.value
	console.log(url)
	this.http.post<any>(url, {"name": this.new_ingredient.value }).subscribe(data => {
		this.http.get<any>(url).subscribe(data => {
  		  this.ingredients = data;
  	  })
  })
 }

 delete_ingr(ingr : String){
   var url = 'http://172.23.1.2:8081/user/' + this.user.value + "/" + ingr
   //var url = 'http://localhost:8081/user/' + this.user.value + "/" + ingr
   var url2 = 'http://172.23.1.2:8081/user/' + this.user.value
   //var url2 = 'http://localhost:8081/user/' + this.user.value
   console.log(url)
   this.http.delete<any>(url).subscribe(data => {
	   this.http.get<any>(url2).subscribe(data => {
           this.ingredients = data;
       })
  })
 }

}
