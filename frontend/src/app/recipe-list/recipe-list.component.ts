import { Component } from '@angular/core';
import { FormControl } from '@angular/forms';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-recipe-list',
  templateUrl: './recipe-list.component.html',
  styleUrls: ['./recipe-list.component.css']
})
export class RecipeListComponent {

   constructor(private http: HttpClient) { }

  recipe = null;
  new_recipe = new FormControl('');

  ngOnInit() {
	  var url = "http://172.23.1.3:8080/recette"
	  //var url = "http://localhost:8080/recette"
	  console.log(url)
      this.http.get<any>(url).subscribe(data => {
		  console.log(data)
          this.recipe = data;
      })
  }
  add_recipe(){
	  var url = "http://172.23.1.3:8080/recette"
	  //var url = "http://localhost:8080/recette"
    this.http.post<any>(url, {"name": this.new_recipe.value }).subscribe(data => {
      this.http.get<any>(url).subscribe(data => {
        console.log(data);
          this.recipe = data;
      })
    })

  }
}
