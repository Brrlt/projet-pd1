import { Component } from '@angular/core';
import { FormControl } from '@angular/forms';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-ingredients-list',
  templateUrl: './ingredients-list.component.html',
  styleUrls: ['./ingredients-list.component.css']
})
export class IngredientsListComponent {

  constructor(private http: HttpClient) { }

 ingredients = null;
 new_ingredient = new FormControl('');

 ngOnInit() {
	 var url = 'http://172.23.1.2:8081'
	 //var url = 'http://localhost:8081'
     this.http.get<any>(url).subscribe(data => {
         this.ingredients = data;
     })
 }
 add_ingredient(){
   var url = 'http://172.23.1.2:8081'
   //var url = 'http://localhost:8081'
   this.http.post<any>(url, {"name": this.new_ingredient.value }).subscribe(data => {
     this.http.get<any>(url).subscribe(data => {
         this.ingredients = data;
     })
   })

 }
}
