import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { IngredientsListComponent } from './ingredients-list/ingredients-list.component';
import { RecipeListComponent } from './recipe-list/recipe-list.component';
import { HomeComponent } from './home/home.component';
import { AccountListComponent } from './account-list/account-list.component';

const routes: Routes = [
  { path: '', component: HomeComponent },
  { path: 'ingredients', component: IngredientsListComponent },
  { path: 'recipe', component: RecipeListComponent },
  { path: 'account', component: AccountListComponent }
];


@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule { }
